/*global window, document, classie, nut, smoothScroll, Blazy*/
(function(win, doc) {

  var head = win.head;

  // Functions
  function scrollTop() {
    if (typeof win.pageYOffset != 'undefined') {
      return win.pageYOffset;
    } else {
      var B = doc.documentBody,
          D = doc.documentElement;
      D = (D.clientHeight) ? D : B;
      return D.scrollTop;
    }
  }

  function addClassOnScroll(elem, threshold, className) {
    className = className || 'scrolled';
    if ( scrollTop() > threshold ) {
      if (!classie.has(elem,className)) { // don't run if the class is already applied on elem	
        classie.add(elem, className);
      }
    } else {
      if ( classie.has(elem, className)) { // only run if the class is applied on the elem	
        classie.remove(elem, className);
      }
    }
  }

  function initCollapse(elem, options) {

    options = options || {};

    var _className = options.className || 'open',
        _handle = nut(options.handle, elem)[0] || nut('.toggle--collapse', elem)[0],
        _body = nut(options.body, elem)[0] || nut('.body--collapse', elem)[0];

    function show() {
      classie.add(elem, _className);

      // Assign dynamic height - animation w/ CSS3
      _body.style.height = 'auto';
      var h = _body.offsetHeight;
      // use number + string concatenation
      // otherwise it won't work in Safari IOS
      _body.style.height = 1 + 'px';

      win.setTimeout(function() {
        _body.style.height = h + 'px';
      });

    }

    function hide() {
      _body.style.height = 'auto';
      var h = _body.offsetHeight;
      _body.style.height = h + 'px';

      win.setTimeout(function() {
        _body.style.height = 0;
      }, 30); // leave delay to copmpute transition start value

      classie.remove(elem, _className);
    }

    function toggleCollapse() {
      if (classie.has(elem, _className)) {
        hide();
      } else {
        show();
      }
    }

    // Initially collapse the panels
    _body.style.display = 'none';
    hide();
    _body.style.display = '';


    // Attach click event on toggle
    _handle.onclick = function() {
      toggleCollapse();
    };
  }

  // When the document is ready, fire the script...
  head.ready(doc,function() {

    // Navbar
    var navbar = doc.getElementById('navbar-main'),
        toggleNav = nut('#toggle-nav', navbar)[0];

    toggleNav.onclick = function() {
      classie.toggle(navbar, 'open');
    };

    // Top button
    var topBtn = doc.getElementById('btn-top'),
        threshold = 900;

    addClassOnScroll(topBtn, threshold); // run on page load

    // Let's play with parallax now
    // We want to animate the image inside the banner
    var banner = nut('.header--page')[0];

    // Create a new instance of parallax on that element
    var myParallax = new win.setPerspectiveOrigin(banner);

    // Usage:
    myParallax.update();

    // Handle scrolling events however you want to update your parallax
    win.onscroll = function() {
      addClassOnScroll(topBtn, threshold);
      myParallax.update();
    };


    // Dropdowns fallback on touch screens
    if (head.touch) {

      var dropdowns = nut('.dropdown'),
          d, c,
          DropdownsCtrl = new DropdownsController();
      // prevent click events from following links on touch screen
      for (d = 0; d < dropdowns.length; d++) {
        for (c = 0; c < dropdowns[d].children.length; c++) {
          var child = dropdowns[d].children[c];
          if (child.nodeName == 'A') {
            // handle click event on anchor
            DropdownsCtrl.registerNode(child);
          }
        }
      }
    }

    function DropdownsController() {
      var ctrl = this;

      this.anchors = [];

      this.lastClicked = [];

      // Methods
      this.registerNode = function(node) {
        this.anchors.push(node);

        node.onclick = function(e) {
          for (var a = 0; a < ctrl.anchors.length; a++) {
            // remove open dropdowns before starting
            classie.remove(ctrl.anchors[a].parentNode, 'open');
          }
          
          var lastPosition = ctrl.lastClicked.length - 1;
          
          if (ctrl.lastClicked[lastPosition] == node) {
            // Go!
          } else {
            e.preventDefault();
            classie.add(node.parentElement, 'open');
          }
          ctrl.lastClicked.push(node);
        };
      };
    }

    // Smooth anchor scrolling
    smoothScroll.init({
      speed: 900,
      updateUrl: true
    });

    // lazy load responsive images
    var bLazy = new Blazy({
      breakpoints: [
        {
          width: 640, // max-width
          src: 'data-src-small'
        }
      ],

      success: function(element) {
        //classie.add(element, 'b-loaded');

        console.log('loaded img');
      }

    });

    var collapseGroups = nut('.collapse-group'),
        j;

    for (j = 0; j < collapseGroups.length; j++) {
      // preserve the scope of the loop by enclosing it in an nanonymous function
      initCollapse(collapseGroups[j], {
        handle: '.header--collapse',
        body: '.body--collapse'
      });
    }
  });
})(window, document);
