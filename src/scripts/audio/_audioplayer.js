/*global window, document, $*/
(function(win, doc) {

  // Modernizr support for audio
  var audioSupport = !!doc.createElement('audio').canPlayType;

  // if audio is supported, create instances of the audioplayer
  if (audioSupport) {

    //head.ready('/assets/js/audio.jquery.min.js', function() {

    $('audio').audioPlayer();
    //});
  }

  //$('audio').jPlayer({})

})(window, document);