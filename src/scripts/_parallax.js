/*global window, document*/

/**
 * Parallax function
 * Takes in a DOM element as first parameter
 * Returns an object with method update()
 */
(function(win, doc) {

  'use strict';

  var docEl = doc.documentElement;

  var setPerspectiveOrigin = function(element) {

    var i,
        supportedPerspectiveOrigin,
        perspectiveOriginProps =  [
          'WebkitPerspectiveOrigin',
          'perspectiveOrigin'
        ];

    // Check for browser vendor prefixes
    for ( i = 0; i < perspectiveOriginProps.length; i++ ) {
      if ( perspectiveOriginProps[i] in docEl.style ) {
        supportedPerspectiveOrigin = perspectiveOriginProps[i];
      }
    }

    function _update() {

      // IE9+ supports innerHeight property, no need for a polyfill
      // since CSS 3d transforms are only in modern browsers
      var winHeight = win.innerHeight,
          winWidth = win.innerWidth,

          elemRect = element.getBoundingClientRect(),

          elemTop = elemRect.top,
          elemBottom = elemRect.bottom,

          // alternatively: element.offsetHeight
          elemHeight = elemBottom - elemTop

      ;

      var updateCondition = function() {
        var verticalCondition =
            elemBottom > 0 &&
            elemTop < winHeight,

            //horizontalCondition =
            //elemRight > 0 &&
            //elemLeft < winWidth,

            // for when we want multiple conditions, e.g. horizontal
            verdict = true
        ;

        verdict = verdict && verticalCondition;

        return verdict;
      };


      // If the element is in view, do parallax - i.e.,
      // if the bottom of that element is below the top of the window
      // AND the top is above the bottom of the window
      // AND the right side of the element is inside the window
      // AND the left side of the element is within the window range
      if ( updateCondition() ) {

        // As the page scrolls, the elements fly by,
        // but we want to keep the perspective-origin at the same level
        //
        // Set the origin to be at the center of the element (2nd term)
        // Safari IOS does not accept mixed values for x/y
        // (-webkit-)perspectiveOrigin, so we convert it to percentages
        var perspOrigin =  (elemTop + elemHeight / 2) / elemHeight * 100;

        // Adjust the perspective-origin (Y)

        if ( supportedPerspectiveOrigin ) {
          element.style[supportedPerspectiveOrigin] = '50% ' + perspOrigin + '%';
        }
      }
    }


    // Public methods
    this.update = _update;

    // Return itself for public use
    return this;

  };

  win.setPerspectiveOrigin = setPerspectiveOrigin;

  return setPerspectiveOrigin;

})(window, document);
