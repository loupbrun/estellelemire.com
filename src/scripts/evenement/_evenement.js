(function($, Packery, window) {
  $(document).ready(function() {
    // initialize lightbox interaction
    $('.magnify-me').magnificPopup({
      type: 'image'
    });
    
    // initialize packery
    $('.packery-gallery').packery({
      itemSelector: '.packery-item'
    });
  });
})(jQuery, Packery, window);