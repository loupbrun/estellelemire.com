/*global window, document, Swipe, classie, nut*/
(function(win, doc) {
  var elem = doc.getElementById('slider'),
      prev = doc.getElementById('slider-previous'),
      next = doc.getElementById('slider-next'),
      slides = elem.getElementsByTagName('div')[0].children,
      bulletsSlider = doc.getElementById('bullets-slider');

  function initToggleSlide(slide) {
    var toggle = nut('.toggle-slider', slide)[0];

    toggle.onclick = function() {
      slider.stop(); // stop the slider
      classie.toggle(slide, 'toggled'); // add `toggled` class to slider
    };
  }

  function addSliderBullets(slider) {
    var length = slider.getNumSlides(), i;

    // Use Swipe API
    for (i = 0; i < length; i++) {
      (function(index) {
        var li = doc.createElement('li'),
            a = doc.createElement('a');

        a.onclick = function() {
          slider.slide(index); // got to the nth-slide
        };

        li.appendChild(a);

        bulletsSlider.appendChild(li);

        a.innerHTML = '&bull;';
      })(i);	
    }
  }

  function updateSliderBullets(slideIndex, bullets) {
    var children = nut('li a', bullets) ,i;

    for (i = 0; i < children.length; i++) {
      (function(n) {
        var a = children[n];

        classie.remove(a, 'active');
      })(i);
    }

    // add active class on nth-bullet
    var current = children[slideIndex];
    classie.add(current, 'active');
  }

  //TouchEmulator();

  var slider = new Swipe(elem, {
    //startSlide: 2,
    speed: 400,
    auto: 7000,
    continuous: true,
    //draggable: true,
    transitionEnd: function(index, elem) {
      updateSliderBullets(index, bulletsSlider);
    }
    //disableScroll: false,
    //stopPropagation: false,
    //callback: function(index, elem) {},
  });


  // Add bullets at the bottom of the slider
  addSliderBullets(slider);

  // Initially highlight bullet 0 (since the slider starts at 0)
  updateSliderBullets(0, bulletsSlider);

  prev.onclick = function() {
    slider.prev();
  };
  next.onclick = function() {
    slider.next();
  };

  // add keyboard interaction
  document.onkeydown = function (e) {
    if (e !== undefined) { // IE7 not handling event
      switch (parseInt(e.which, 10)) {
        case 37: // left arrow
          slider.prev();
          break;
        case 39: // right arrow
          slider.next();
          break;
      }
    }
  };

  // attach click events on each slide toggle button
  for (var i = 0; i < slides.length; i++) {
    initToggleSlide(slides[i]);
  }

})(window, document);