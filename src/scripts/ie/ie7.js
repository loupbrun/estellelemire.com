/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'elemire-icons\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-note-3': '&#xe60a;',
		'icon-note-all': '&#xe610;',
		'icon-audio': '&#xe611;',
		'icon-bague': '&#xe612;',
		'icon-bio': '&#xe613;',
		'icon-e-mail': '&#xe614;',
		'icon-home': '&#xe615;',
		'icon-note-1': '&#xe608;',
		'icon-note-2': '&#xe609;',
		'icon-note-4': '&#xe60b;',
		'icon-note-5': '&#xe60c;',
		'icon-note-6': '&#xe60d;',
		'icon-note-7': '&#xe60e;',
		'icon-note-8': '&#xe60f;',
		'icon-angle-down': '&#xe600;',
		'icon-angle-left': '&#xe601;',
		'icon-angle-right': '&#xe602;',
		'icon-angle-up': '&#xe603;',
		'icon-close': '&#xe604;',
		'icon-plus': '&#xe605;',
		'icon-tech-1': '&#xe606;',
		'icon-tech-2': '&#xe607;',
		'icon-link': '&#xe005;',
		'icon-image': '&#xe010;',
		'icon-video': '&#xe018;',
		'icon-paragraph': '&#xe025;',
		'icon-play': '&#xe052;',
		'icon-pause': '&#xe053;',
		'icon-volume': '&#xe098;',
		'icon-mute': '&#xe099;',
		'icon-hamburger': '&#xe120;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
