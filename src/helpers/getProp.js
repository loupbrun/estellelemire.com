'use strict';

module.exports.register = function (Handlebars, options, params) {
  Handlebars.registerHelper('getProp', function(obj, key) {
    return obj[key];
  });
};