Estelle Lemire
===

Répertoire du code source du site web officiel d'[Estelle Lemire](http://estellelemire.com).

### Installation

Ce site est construit avec [Grunt](http://gruntjs.com) ([Node.js](https://nodejs.org/)) et [Jekyll](http://jekyllrb.com) ([Ruby](https://www.ruby-lang.org/)). Les dépendances sont gérées avec [Bower](http://bower.io). Les fichiers `CSS` sont générés avec [SASS](http://sass-lang.com/).

&copy;2015 Estelle Lemire. Tous droits réservés.
