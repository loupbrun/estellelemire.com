.PHONY: build

help:
	@echo "Usage: make <command>"
	@echo "  serve  Runs a development webserver on port "
	@echo "  build   Build the site with minification"
	@echo "  build-staging   Build the site with *staging* configuration"

serve:
	grunt dev

build:
	grunt

all:
	grunt && ./deploy.sh
