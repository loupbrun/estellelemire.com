/*global module, require*/
module.exports = function ( grunt ) {
  
  var sass = require('sass');

  // Take care of all Grunt plugins in a single line
  require('load-grunt-tasks')(grunt, { pattern: ['grunt-*', '!grunt-assemble-*', 'grunt-sass'] });

  // This is the configuration object Grunt uses to give each plugin its instructions.
  grunt.initConfig({

    // Read our `package.json` file so we can access the package name and version. It's already there, so we won't repeat ourselves here.
    pkg: grunt.file.readJSON("package.json"),

    // Setup config
    config: {
      src: 'src',
      temp: '.tmp',
      lib: 'src/assets/lib',
      dist: 'dist'
    },

    clean: {
      temp: ['<%= config.temp %>'],
      dist: ['<%= config.dist %>']
    },

    copy: {
      temp: {
        files: [
          {
            expand: true,
            flatten: false,
            cwd: '<%= config.src %>/assets',
            src: [
              '**',
            ],
            dest: '<%= config.temp %>/assets'
          },
          {
            expand: true,
            flatten: true,
            dot: true,
            cwd: '<%= config.src %>',
            src: ['.htaccess', 'robots.txt'],
            dest: '<%= config.temp %>'
          }
        ]
      },
      dist: {
        files: [
          {
            expand: true,
            flatten: false,
            cwd: '<%= config.temp %>/assets',
            src: [
              '**',
              '!**scripts/*',
            ],
            dest: '<%= config.dist %>/assets'
          },
          {
            expand: true,
            flatten: true,
            dot: true,
            cwd: '<%= config.temp %>',
            src: ['.htaccess', 'robots.txt'],
            dest: '<%= config.dist %>'
          }
        ]
      }
    },

    // concatenate js files
    concat: {
      options: {
        separator: ';',
        banner:
        '/** \n' +
        ' * @author <%= pkg.author %> \n' +
        ' * @version <%= pkg.version %> \n' +
        ' * @description <% pkg.description %> \n' +
        ' * @license <% pkg.license %> \n' +
        ' */ '
      },
      main: {
        src: [
          '<%= config.lib %>/classie/classie.js',
          '<%= config.lib %>/nut/src/nut.js', // cross-browser selector
          '<%= config.lib %>/smooth-scroll/dist/js/smooth-scroll.js',
          '<%= config.lib %>/FastActive/FastActive.js', // adds reactive responsiveness to touch. Auto-instantiates itself
          '<%= config.lib %>/blazy/blazy.js',

          '<%= config.src %>/scripts/_parallax.js',
          '<%= config.src %>/scripts/_general.js'
        ],
        dest: '<%= config.temp %>/assets/scripts/main.js'
      },

      slider: {
        src: [
          '<%= config.lib %>/swipe/swipe.js',

          // Custom scripts
          '<%= config.src %>/scripts/slider/_slider.js'
        ],
        dest: '<%= config.temp %>/assets/scripts/slider.js'
      },

      audio: {
        files: [
          {
            '<%= config.temp %>/assets/scripts/audioplayer.js':
            [
              '<%= config.lib %>/jquery/dist/jquery.js',

              // Custom scripts
              '<%= config.src %>/scripts/audio/audio.jquery.js',
              '<%= config.src %>/scripts/audio/_audioplayer.js'
            ]	
          }
        ]
      },

      evenement: {
        files: [
          {
            '<%= config.temp %>/assets/scripts/evenement.js':
            [
              '<%= config.lib %>/jquery/dist/jquery.js',
              '<%= config.lib %>/magnific-popup/dist/jquery.magnific-popup.js',
              '<%= config.lib %>/packery/dist/packery.pkgd.js',

              '<%= config.src %>/scripts/_evenement.js'
            ]
          }
        ]
      },

      ie: {
        files: [
          {
            '<%= config.temp %>/assets/scripts/ie.js':
            [
              '<%= config.src %>/scripts/ie/*.js'
            ]
          }
        ]
      }
    },

    uglify: {
      js: {
        files: [{
          expand: true,
          cwd: '<%= config.temp %>/assets/scripts',
          src: '*.js',
          dest: '<%= config.dist %>/assets/scripts'
        }]
      },
    },

    imagemin: {
      evenement2015: {
        files: [{
          expand: true,
          cwd: '<%= config.src %>/assets/img/ondes-au-conservatoire-2015',
          src: '*.jpg',
          dest: '<%= config.src %>/assets/img/ondes-au-conservatoire-2015/min'
        }]
      }
    },

    sass: {
      main: {
        files: {
          '<%= config.temp %>/assets/styles/main.css': '<%= config.src %>/styles/main.scss',
          '<%= config.src %>/templates/partials/main.css.hbs': '<%= config.src %>/styles/main.scss'
        },
      },
      landing: {
        files: {
          '<%= config.temp %>/assets/styles/landing.css': '<%= config.src %>/styles/landing.scss',
          '<%= config.src %>/templates/partials/landing.css.hbs': '<%= config.src %>/styles/landing.scss'
        }
      },

      ie: {
        files: {
          '<%= config.temp %>/assets/styles/ie.css': '<%= config.src %>/styles/ie.scss'
        }
      },
      options: {
        outputStyle: 'compressed',
        implementation: sass
      }
    },

    postcss: {
      options: {
        map: false,
        processors: [
          require('autoprefixer')({
            browsers: ['last 2 versions', 'IE 7']
          })
        ]
      },
      temp: {
        src: [
          '<%= config.temp %>/assets/styles/*.css',
          '<%= config.src %>/templates/partials/main.css.hbs',
          '<%= config.src %>/templates/partials/landing.css.hbs']
      }
    },

    // watch: rebuild parts of site on file change
    watch: {
      sass: {
        files: ['<%= config.src %>/styles/**/*.scss'],
        tasks: ['sass']
      },

      js: {
        files: ['<%= config.src %>/scripts/**/*.js'],
        tasks: ['concat', 'copy:temp']
      },
      assets: {
        files: ['<%= config.src %>/assets/**'],
        tasks: ['copy:temp']
      },
      assemble: {
        files: ['<%= config.src %>/templates/**/*.{md,hbs}'],
        tasks: ['assemble']
      },
      ivereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= config.src %>/templates/**/*.{md,hbs}',
          '<%= config.src %>/assets/**',
          '<%= config.src %>/scripts/**/*.js',
          '<%= config.src %>/styles/*.scss',
        ]
      }
    },

    connect: {
      options: {
        port: 9000,
        open: true,
        livereload: 35729,
        // Change this to '0.0.0.0' to access the server from outside
        hostname: 'localhost'
      },
      server: {
        options: {
          base: '<%= config.temp %>'
        }
      },
      livereload: {
        options: {
          middleware: function(connect) {
            return [
              connect.static('<%= config.temp %>')
            ];
          }
        }
      }
    },

    assemble: {
      options: {
        helpers: ['<%= config.src %>/helpers/*.js'],
        marked: { sanitize: false },
        flatten: true,
        data: ['<%= config.src %>/data/**/*.{json,yml}'],
        partials: ['<%= config.src %>/templates/partials/*.hbs'],
        layoutdir: '<%= config.src %>/templates/layouts',
        layout: 'normal.hbs',
        baseurl: '',
        plugins: ['grunt-assemble-permalinks', 'grunt-assemble-i18n'],
        permalinks: {
          stripnumber: true,
          structure: ':language/:basename/index:ext'
        }
      },
      pages: {
        options: {
          i18n: {
            data: ['<%= config.src %>/data/i18n.yml', '<%= config.src %>/data/i18n/*.yml'],
            templates: ['<%= config.src %>/templates/pages/*.hbs']
          },
        },
        files: [{
          src: '!*.*',
          dest: '<%= config.temp %>'
        }]
      },
      ondes: {
        options: {
          i18n: {
            data: ['<%= config.src %>/data/i18n.yml', '<%= config.src %>/data/i18n/*.yml'],
            templates: ['<%= config.src %>/templates/pages/ondes/*.hbs']
          },
          permalinks: {
            stripnumber: true,
            structure: ':language/:categories/:basename/index:ext'
          }
        },
        files: [{
          src: '!*.*',
          dest: '<%= config.temp %>'
        }]
      },
      audio: {
        options: {
          i18n: {
            data: ['<%= config.src %>/data/i18n.yml', '<%= config.src %>/data/i18n/*.yml'],
            templates: ['<%= config.src %>/templates/pages/audio/*.hbs']
          },
          permalinks: {
            stripnumber: true,
            structure: ':language/:categories/:basename/index:ext'
          }
        },
        files: [{
          src: '!*.*',
          dest: '<%= config.temp %>'
        }]
      },
      landing: {
        options: {
          i18n: {
            data: ['<%= config.src %>/data/i18n.yml', '<%= config.src %>/data/i18n/*.yml']
          },
          permalinks: {
            structure: 'index:ext'
          }
        },
        files: [{
          src: '<%= config.src %>/templates/pages/other/landing.hbs',
          dest: '<%= config.temp %>'
        }]
      },
      errorDocument: {
        options: {
          i18n: {
            data: ['<%= config.src %>/data/i18n.yml', '<%= config.src %>/data/i18n/*.yml']
          },
          permalinks: {
            structure: ':basename/index:ext'
          }
        },
        files:[{
          src: '<%= config.src %>/templates/pages/other/404.md.hbs',
          dest: '<%= config.temp %>'
        }]
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseBooleanAttributes: false,
          collapseWhitespace: true,
          conservativeCollapse: true,
          removeAttributeQuotes: false,
          removeComments: true,
          removeCommentsFromCDATA: true,
          removeEmptyAttributes: false,
          removeOptionalTags: false,
          removeRedundantAttributes: true,
          useShortDoctype: true
        },
        files: [{
          expand: true,
          cwd: '<%= config.temp %>',
          src: '{,**/}*.html',
          dest: '<%= config.dist %>'
        }]
      },
    }
  });

  grunt.registerTask(
    'temp',
    'Build the site without minification', 
    [
      'clean:temp',
      'copy:temp',
      'concat',
      'sass',
      'postcss',
      'assemble'
    ]
  );


  grunt.registerTask(
    'server',
    'Serve the site and auto-regenerate on file change.',
    [
      'temp',
      'connect:server',
      'watch'
    ]
  );
  grunt.registerTask(
    'default',
    'Default task - build the site ready for production.',
    ['dist']
  );

  grunt.registerTask(
    'dist',
    'Build a site ready for production',
    [
      'temp',
      'clean:dist',
      'copy:dist',
      'uglify',
      'htmlmin'
    ]
  );

};
